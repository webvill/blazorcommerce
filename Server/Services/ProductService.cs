using BlazorCommerce.Shared.Models;
using Microsoft.EntityFrameworkCore;
using BlazorCommerce.Server.Data;
namespace BlazorCommerce.Server.Services
{
    public class ProductService : IProductService
    {
        private readonly DataContext _context;
        public ProductService(DataContext context)
        {
            _context = context;
        }

        public async Task<ServiceResponse<Product>> GetProductAsync(int id)
        {
            var response = new ServiceResponse<Product>
            {
                Data = await _context.Products.FindAsync(id)
            };
            return response;
        }


        public async Task<ServiceResponse<List<Product>>> GetProductsAsync()
        {
            var response = new ServiceResponse<List<Product>>
            {
                Data = await _context.Products.ToListAsync()
            };
            return response;
        }

        public Task<ServiceResponse<List<Product>>> GetProductsAsync(int id)
        {
            throw new NotImplementedException();
        }
    }
}