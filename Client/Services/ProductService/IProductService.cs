namespace BlazorCommerce.Client.Services
{
    public interface IProductService
    {
        public List<Product> Products { get; set; }
        Task GetProducts();
    }
}