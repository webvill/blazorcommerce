namespace BlazorCommerce.Shared.Models
{
    public class Product
    {
        public int Id { get; set; }
        public string Title { get; set; } = string.Empty;
        public string Description { get; set; } = string.Empty;
        public string ImageUrl { get; set; } = string.Empty;
        public decimal Price { get; set; }
        public int Discount { get; set; }
        public bool Featured { get; set; } = false;
        public Category? Catetory { get; set; }
        public int CategoryId { get; set; }
        

    }
}